package a202sgi.runassistant;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.os.Handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import android.widget.Toast;

public class FirstFragment extends Fragment {
    public static FirstFragment newInstance() {
        FirstFragment fragment = new FirstFragment();
        return fragment;
    }

    TextView textView;
    Button start, reset, save, pause;
    long MillSec, StartTime, TimeBuffer, TimeUpdate = 0L;
    int Secs, Mins, MillSecs;
    Handler timeHandler;
    ListView RecentListView;
    String[] TimeListItems = new String[] {  };
    List<String> TimeListItemsArray ;
    ArrayAdapter<String> TimeListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment1, container, false);

        textView = (TextView) v.findViewById(R.id.timer);
        start = (Button) v.findViewById(R.id.startButton);
        reset = (Button) v.findViewById(R.id.resetButton);
        pause = (Button) v.findViewById(R.id.pauseButton);
        save = (Button) v.findViewById(R.id.saveButton);
        RecentListView = (ListView)v.findViewById(R.id.RecentTimes);

        TimeListItemsArray = new ArrayList<String>(Arrays.asList(TimeListItems));

        TimeListAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, TimeListItemsArray);

        RecentListView.setAdapter(TimeListAdapter);

        timeHandler = new Handler();

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StartTime = SystemClock.uptimeMillis();
                timeHandler.postDelayed(runnable, 0);

                reset.setEnabled(false);
            }
        });

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TimeBuffer += MillSec;

                timeHandler.removeCallbacks(runnable);

                reset.setEnabled(true);

            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MillSec = 0L;
                StartTime = 0L;
                TimeBuffer = 0L;
                TimeUpdate = 0L;
                Secs = 0;
                Mins = 0;
                MillSecs = 0;

                textView.setText("0:00:000");

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TimeListItemsArray.add(textView.getText().toString());

                TimeListAdapter.notifyDataSetChanged();
                //Toast.makeText(v.getContext(), "Recent Time Has been Saved!", Toast.LENGTH_SHORT).show();

            }
        });

        return v;
    }

    public Runnable runnable = new Runnable() {

        public void run() {

            MillSec = SystemClock.uptimeMillis() - StartTime;

            TimeUpdate = TimeBuffer + MillSec;

            Secs = (int) (TimeUpdate / 1000);

            Mins = Secs / 60;

            Secs = Secs % 60;

            MillSecs = (int) (TimeUpdate % 1000);

            textView.setText("" + Mins + ":"
                    + String.format(Locale.ENGLISH, "%02d", Secs) + ":"
                    + String.format(Locale.ENGLISH, "%03d", MillSecs));

            timeHandler.postDelayed(this, 0);

        }
    };
}